(defproject menu
            "0.1.0-SNAPSHOT"
            :description "FIXME: write description"
            :url "http://example.com/FIXME"
            :dependencies [[log4j "1.2.17"
                            :exclusions [javax.mail/mail
                                         javax.jms/jms
                                         com.sun.jdmk/jmxtools
                                         com.sun.jmx/jmxri]]
                           [selmer "0.7.7"]
                           [com.taoensso/tower "3.0.2"]
                           [reagent-forms "0.2.9"]
                           [prone "0.6.1"]
                           [org.webjars/react "0.11.2"]
                           [org.webjars/bootstrap "3.2.0"]
                           [cljs-http "0.1.16"]
                           [compojure "1.1.9"]
                           [liberator "0.12.2"]
                           [fogus/ring-edn "0.2.0"]
                           [figwheel "0.1.5-SNAPSHOT"]
                           [com.cemerick/piggieback "0.1.3"]
                           [weasel "0.4.2"]
                           [garden "1.2.5"]
                           [markdown-clj "0.9.58" :exclusions [com.keminglabs/cljx]]
                           [im.chit/cronj "1.4.3"]
                           [com.taoensso/timbre "3.3.1"]
                           [org.postgresql/postgresql "9.3-1102-jdbc41"]
                           [cljs-ajax "0.3.3"]
                           [noir-exception "0.2.3"]
                           [korma "0.4.0"]
                           [org.clojure/clojurescript "0.0-2371"]
                           [lib-noir "0.9.5"]
                           [org.clojure/clojure "1.6.0"]
                           [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                           [environ "1.0.0"]
                           [ring-server "0.3.1"]
                           [secretary "1.2.1"]
                           [ragtime "0.3.6"]]
            :jvm-opts ["-server"]
            :source-paths ["src" "src-styles" "resource"]
            :plugins [[lein-ring "0.8.13"]
                      [lein-environ "1.0.0"]
                      [lein-ancient "0.5.5"]
                      [lein-cljsbuild "1.0.3"]
                      [ragtime/ragtime.lein "0.3.6"]
                      [lein-cucumber "1.0.2"]
                      [lein-garden "0.2.5"]]
            :ring {:handler menu.handler/app,
                   :init    menu.handler/init,
                   :destroy menu.handler/destroy}
            :profiles {:uberjar {:cljsbuild   {:jar true,
                                               :builds
                                                    {:app
                                                     {:source-paths ["env/prod/cljs"],
                                                      :compiler     {:optimizations :advanced, :pretty-print false}}}},
                                 :hooks       [leiningen.cljsbuild],
                                 :omit-source true,
                                 :env         {:production true},
                                 :aot         :all},
                       :production
                                {:ring
                                 {:open-browser? false, :stacktraces? false, :auto-reload? false}},
                       :dev     {:cljsbuild {:builds {:app {:source-paths ["env/dev/cljs"]}}},
                                 :dependencies
                                               [[ring/ring-devel "1.3.2"]
                                                [org.clojure/core.cache "0.6.3"]
                                                [pjstadig/humane-test-output "0.6.0"]
                                                [ring-mock "0.1.5"]
                                                [clj-webdriver/clj-webdriver "0.6.1"]],
                                 :injections
                                               [(require 'pjstadig.humane-test-output)
                                                (pjstadig.humane-test-output/activate!)],
                                 :repl-options {:init-ns menu.repl
                                                :nrepl-middleware
                                                         [cemerick.piggieback/wrap-cljs-repl]}
                                 :plugins      [[lein-figwheel "0.1.4-SNAPSHOT"]]
                                 :figwheel     {:http-server-root "public"
                                                :port             3449}
                                 :env          {:dev true}}}
            :ragtime {:migrations ragtime.sql.files/migrations,
                      :database
                                  "jdbc:postgresql://localhost/menu?user=db_user_name_here&password=db_user_password_here"}
            :cljsbuild {:builds
                        {:app
                         {:source-paths ["src-cljs"],
                          :compiler
                                        {:output-dir    "resources/public/js/out",
                                         :externs       ["react/externs/react.js"],
                                         :optimizations :none,
                                         :output-to     "resources/public/js/app.js",
                                         :source-map    "resources/public/js/out.js.map",
                                         :pretty-print  true}}}}
            :garden {:builds [{;; Optional name of the build:
                               :id "screen"
                               ;; Source paths where the stylesheet source code is
                               :source-paths ["src-styles"]
                               ;; The var containing your stylesheet:
                               :stylesheet menu.core/screen
                               ;; Compiler flags passed to `garden.core/css`:
                               :compiler {;; Where to save the file:
                                          :output-to "resources/public/css/screen.css"
                                          ;; Compress the output?
                                          :pretty-print? false}}]}
            :uberjar-name "menu.jar"
            :cucumber-feature-paths ["test/features/"]
            :min-lein-version "2.0.0"
            :global-vars {*print-length* 20})