(ns menu.routes.home
  (:require [menu.layout :as layout]
            [menu.util :as util]
            [menu.style.core :as style]
            [compojure.core :refer :all]
            [noir.response :refer [edn]]
            [clojure.pprint :refer [pprint]]
            [ring.util.response :refer (redirect)]
            [garden.core :refer [css]]))

(defn home-page []
  (layout/render
    "app.html" {:docs (util/md->html "/md/docs.md")}))

(defn save-document [doc]
  (pprint doc)
  {:status "ok"})

(defroutes home-routes
  (GET "/is-dev" []
       {:status  200
        :body    (pr-str util/dev?)
        :headers {"Content-Type" "application/edn"}})

  (GET "/*.css" {{path :*} :route-params}
       (style/css-data path util/dev?))

  (GET "/greeting" []
       "Hello World!")

  (GET "/" [] (home-page))
  (POST "/save" {:keys [body-params]}
        (edn (save-document body-params))))
