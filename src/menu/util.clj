(ns menu.util
  (:require [noir.io :as io]
            [environ.core :refer [env]]
            [markdown.core :as md]))

(defn md->html
  "reads a markdown file from public/md and returns an HTML string"
  [filename]
  (md/md-to-html-string (io/slurp-resource filename)))

(def dev? (let [setting (env :dev)]
               (if (string? setting)
                 (Boolean/parseBoolean setting)
                 setting)))
