# menu



* issue tracking
http://mdk.myjetbrains.com/youtrack/dashboard

Built with

$ lein new luminus menu +site +cucumber +postgres +cljs



* dev workflow

# generate JavaScript once

$ lein cljsbuild once

# start figwheel

$ lein figwheel

$ lein ring server

Create a repl inside IntelliJ Idea

Start a browser repl:
(brepl)

Generate css using garden:
$ lein garden once


## Running

To start a web server for the application, run:

    lein ring server


* Production

Start project without figwheel and weasel:

Use normal lein cljsbuild +

IS_DEV=false lein ring server

*



# Run tests

$ lein test

# Run cucumber tests



