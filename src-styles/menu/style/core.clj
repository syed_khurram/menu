(ns menu.style.core
  (:require [garden.def :refer [defstylesheet defstyles]]
            [garden.units :refer [px]]
            [clojure.java.io :as io]
            [garden.core :refer [css]]))

;; Change defstylesheet to defstyles.
(defstyles screen
           #_[:body
            {:font-family "sans-serif"
             :font-size (px 16)
             :line-height 1.5}]
           [:.container [:.row {:margin-bottom (px 20)}]])

;; http://mdk.myjetbrains.com/youtrack/issue/PDF-1
(defn css-page [path]
      (when-let [garden-url
                 (io/resource (str "public/" path ".garden"))]
                (let [garden-data (load-file (.getPath garden-url))]
                     {:status  200
                      :headers {"Content-Type" "text/css"}
                      :body    (css garden-data)})))

(def css-page-memoized (memoize css-page))

;; On production, we don't want to dynamically generate
;; css from garden. We'd want to use garden to compile and save
;; css into the file system and serve the .css and .js from
;; a cdn.

(defn css-data [path is-dev?]
      (if is-dev?
        (css screen)
        (css-page-memoized path)))
