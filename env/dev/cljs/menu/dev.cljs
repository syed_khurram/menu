(ns menu.app
  (:require-macros [cljs.core.async.macros :refer (go)])
  (:require [menu.core :as core]
            [cljs-http.client :as http]
            [figwheel.client :as fw]
            [weasel.repl :as ws-repl]))

(enable-console-print!)

(defonce init-dev
         ;; we need to defonce this, so it won't be executed again upon code
         ;; reload
         (go (let [body (:body (<! (http/get "/is-dev")))]
               (when (= body true) ;; has to match exactly true and not some string
                 (fw/watch-and-reload
                   :websocket-url   "ws://localhost:3449/figwheel-ws"
                   :jsload-callback
                   (fn []
                     (println "reloaded")))
                 (ws-repl/connect "ws://localhost:9001" :verbose true)))))

(.log js/console "inside dev env")
(core/init!)

